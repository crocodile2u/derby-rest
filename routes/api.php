<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function (Request $request) {
    return "home";
});
Route::get('/table/{name}', function (Request $request) {
    $tableName = $request->route()->parameter("name");
    return response()->stream(function () use ($tableName) {
        passthru("FORMAT=json dump.sh olca $tableName");
    });
});

Route::get('/query/{query}/{columns?}', function (Request $request) {
    $query = $request->route()->parameter("query");
    $columns = $request->route()->parameter("columns");
    return response()->stream(function () use ($query, $columns) {
        $q = escapeshellarg($query);
        $cols = escapeshellarg($columns);
        passthru("FORMAT=json query.sh olca $q $cols");
    });
});
