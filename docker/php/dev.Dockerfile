FROM crocodile2u/derby-dumper

RUN apk add --no-cache php-session php-tokenizer php-mbstring

VOLUME /app
